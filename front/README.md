## Cobranzas Frontend

```
after cloning the repository run: yarn install
```

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### Redux

In order to connect the REST API with the React project we are using _redux_ you can check for basic examples here
[REDUX](https://react-redux.js.org/introduction/basic-tutorial)

- actions: we are going to call the restAPI endpoints there
- reducers: we are going to resolve and connect the actions with the frontend there
- containers: those are react components that are connected to the redux for consuming the API data provided

### Node OracleDB

For consuming the DB we are using [node-oracleDB](https://oracle.github.io/node-oracledb)

### Styles

For our own styles we are using [material-ui](https://material-ui.com/getting-started/usage/), you can check there
on the _Components_ section for more info