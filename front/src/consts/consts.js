/**
 * this are the header cells on the exception table
 */
export const headCells = [
  { id: "USR_ID", label: "USR_ID" },
  { id: "USR_MAIL_ADDRESS", label: "USR_MAIL_ADDRESS" },
  { id: "USR_NAME", label: "USR_NAME" },
  { id: "USR_SURNAME", label: "USR_SURNAME" },
  { id: "USR_END_DATE", label: "USR_END_DATE" },
  { id: "USR_START_DATE", label: "USR_START_DATE" },
];
