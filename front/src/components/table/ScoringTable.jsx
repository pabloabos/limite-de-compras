import React from "react";
import PropTypes from "prop-types";
import { Table, TableContainer, TablePagination } from "@material-ui/core";

import TableHeader from "./TableHeader";
import BodyTable from "./TableBody";
import { headCells } from "../../consts/consts";
import { getComparator, stableSort } from "./functions";
import "./Styles.scss";

const ScoringTable = (props) => {
  const [order, setOrder] = React.useState("asc");
  const [orderBy, setOrderBy] = React.useState("USR_ID");
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(20);

  /**
   * sort the table by the property clicked ascending/descending
   * @param {object} event
   * @param {string} property
   */
  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  /**
   * Select all the users on the table
   * @param {object} event
   */
  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = props.users.map((n) => n.name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  /**
   * select a single user based on the name clicked
   * @param {object} event
   * @param {string} name
   */
  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }

    setSelected(newSelected);
  };

  /**
   * change the page of the table
   * @param {object} event
   * @param {number} newPage
   */
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
    props.getUsers(
      parseInt(rowsPerPage * newPage, 10),
      parseInt(rowsPerPage * 2 * newPage, 10)
    );
  };

  /**
   * check the number of users to show in the table
   * @param {object} event
   */
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  /**
   * check if a user is selected based on the USR_ID
   * @param {string} USR_ID
   */
  const isSelected = (USR_ID) => selected.indexOf(USR_ID) !== -1;

  return (
    <div className={"root"}>
      <TableContainer>
        <Table
          className={"table"}
          aria-labelledby="tableTitle"
          aria-label="enhanced table"
        >
          <TableHeader
            headCells={headCells}
            numSelected={selected.length}
            order={order}
            orderBy={orderBy}
            onSelectAllClick={handleSelectAllClick}
            onRequestSort={handleRequestSort}
            rowCount={props.users.length}
          />
          <BodyTable
            stableSort={stableSort}
            getComparator={getComparator}
            page={page}
            rowsPerPage={rowsPerPage}
            isSelected={isSelected}
            handleClick={handleClick}
            order={order}
            orderBy={orderBy}
            users={props.users}
          />
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 20, 50]}
        component="div"
        count={props.usersCount}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </div>
  );
};

ScoringTable.propTypes = {
  users: PropTypes.array.isRequired,
  getUsers: PropTypes.func.isRequired,
  usersCount: PropTypes.number.isRequired,
};

export default ScoringTable;
