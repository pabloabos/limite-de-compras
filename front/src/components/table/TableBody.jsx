import React from "react";
import PropTypes from "prop-types";
import { TableBody, TableCell, TableRow, Checkbox } from "@material-ui/core";

const BodyTable = (props) => {
  const {
    stableSort,
    getComparator,
    page,
    rowsPerPage,
    isSelected,
    handleClick,
    order,
    orderBy,
  } = props;
  return (
    <TableBody>
      {stableSort(props.users, getComparator(order, orderBy))
        .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
        .map((row, index) => {
          const isItemSelected = isSelected(row.USR_ID);
          const labelId = `enhanced-table-checkbox-${index}`;

          return (
            <TableRow
              hover
              onClick={(event) => handleClick(event, row.USR_ID)}
              role="checkbox"
              aria-checked={isItemSelected}
              tabIndex={-1}
              key={index}
              selected={isItemSelected}
            >
              <TableCell padding="checkbox">
                <Checkbox
                  checked={isItemSelected}
                  inputProps={{ "aria-labelledby": labelId }}
                />
              </TableCell>
              <TableCell component="th" id={labelId} scope="row" padding="none">
                {row.USR_ID}
              </TableCell>
              <TableCell>{row.USR_MAIL_ADDRESS}</TableCell>
              <TableCell>{row.USR_NAME}</TableCell>
              <TableCell>{row.USR_SURNAME}</TableCell>
              <TableCell>{row.USR_END_DATE}</TableCell>
              <TableCell>{row.USR_START_DATE}</TableCell>
            </TableRow>
          );
        })}
    </TableBody>
  );
};

BodyTable.propTypes = {
  users: PropTypes.array.isRequired,
};

export default BodyTable;
