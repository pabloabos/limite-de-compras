import React from "react";
import LogoClaro from "../../assets/img/Claro_logo.png";
import { AppBar, AppBarTitle, AppBarLogo } from "@claro/common-ui-components";

const Header = () => {
  return (
    <AppBar
      style={{
        position: "relative",
      }}
    >
        <AppBarLogo
          src={LogoClaro}
          width={65}
          height={25}
          alt="Claro"
          onClick={() => alert("Volver")}
        />
        <AppBarTitle>Excepción Límite de Compras</AppBarTitle>
    </AppBar>
  );
};

export default Header;
