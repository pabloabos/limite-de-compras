import React from "react";
import Grid from "@material-ui/core/Grid";
import SearchArchive from "../searchArchive/SearchArchive";
import SearchClient from "../searchClient/SearchClient";
// import ScoringResult from "../tableresult/ScoringResult";

const FileForm = () => {
  return (
    <Grid container>
      <Grid item xs={6} md={4} lg={4}>
        <SearchClient />
      </Grid>
      <Grid item xs={6} md={4} lg={4}>
        <SearchArchive />
      </Grid>
    </Grid>
  );
};

export default FileForm;
