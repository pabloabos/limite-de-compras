import React, { useState } from "react";
import {
  Typography,
  Modal,
  ModalHeader,
  ModalHeaderTitle,
  TransparentBackIconButton,
  ModalHeaderActions,
  Info,
  Close,
  ModalFooter,
  ModalContent, Divider
} from "@claro/common-ui-components";


const TextModal = () => {
  const [modalAbierto, setModalAbierto] = useState(false);

  return (
    <>
      <Modal open={modalAbierto} onCloseRequest={() => setModalAbierto(false)}>
        <ModalHeader>
          <ModalHeaderTitle>
            <Typography Component="p" variant="body1">
              <strong> Formato de Archivo</strong>
            </Typography>
          </ModalHeaderTitle>
          <ModalHeaderActions>
            <TransparentBackIconButton onClick={() => setModalAbierto(false)}>
              <Close size={25} />
            </TransparentBackIconButton>
          </ModalHeaderActions>
        </ModalHeader>
        <ModalContent >
          <Typography Component="p" variant="subtitle2" className="justify">
            Formato archivo excel, guardar con extension CSV. El nombre del
            archivo no debe ser mayor a 30 caracteres.
            <br /> Se aconseja como nombre de archivo: EXC_DDMMYYYY.txt
            <Divider />
             Campos: <br /> Clt_id : Maximo 10 caracteres de ancho
              <br />
              Límite: Maximo 10 carácter de ancho
              <br />
              Fecha Caducidad: DD/MM/YYYY (DD: día, MM: Mes, YYYY: Año)
              <br />
              Operación: 1 carácter (A/B/M)

          </Typography>
        </ModalContent>
        <ModalFooter separator>
          <Typography Component="p" variant="body1">
            Ejemplo: 27484;1500;31/12/2999;A
          </Typography>
        </ModalFooter>
      </Modal>
      <Info onClick={() => setModalAbierto(true)} />
    </>
  );
};

export default TextModal;
