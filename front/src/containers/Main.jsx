import React, { Component } from "react";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import { LoadingClaro } from "@claro/common-ui-components";

import { getAllUsers, getUsersCount } from "../actions/users";
import ScoringTable from "../components/table/ScoringTable";
import FileForm from "../components/form/Form";

class Main extends Component {
  componentDidMount() {
    this.props.getAllUsers();
    this.props.getUsersCount();
  }

  render() {
    return (
      <Grid container spacing={3}>
        <Grid item xs={12}>
          {this.props.users.length !== 0 && this.props.usersCount !== 0 ? (
            <ScoringTable
              users={this.props.users}
              getUsers={this.props.getAllUsers}
              usersCount={this.props.usersCount[0].usersNumber}
            />
          ) : (
            <LoadingClaro
              imageSrc={""}
              imageWidth={0}
              imageHeight={0}
              text={"cargando"}
              overlay
              fullViewportSize
            />
          )}
        </Grid>
        <Grid item xs={12}>
          <FileForm />
        </Grid>
      </Grid>
    );
  }
}

function mapStateToProps(state) {
  return {
    users: state.users.users,
    usersCount: state.users.usersCount,
  };
}

export default connect(mapStateToProps, { getAllUsers, getUsersCount })(Main);
