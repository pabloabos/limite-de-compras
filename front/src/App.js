import React from "react";
import Grid from "@material-ui/core/Grid";
import Main from "./containers/Main";
import Header from "./components/header/Header";
import "./App.scss";

function App() {
  return (
    <div className="App">
      <Grid container spacing={3}>
        <Grid item xs={12} className="header">
          <Header />
        </Grid>
        <Grid item xs={12} className="main">
          <Main />
        </Grid>
      </Grid>
    </div>
  );
}

export default App;
