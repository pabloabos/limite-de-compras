import axios from "axios";
axios.defaults.headers.post["Content-Type"] = "application/json;charset=utf-8";
axios.defaults.headers.post["Access-Control-Allow-Origin"] = "*";

export const GET_USERS = "GET_USERS";
export const GET_USERS_COUNT = "GET_USERS_COUNT";

export function getAllUsers(from = 0, to = 20) {
  return function (dispatch) {
    return axios
      .get(`http://localhost:8080/users?from=${from}&to=${to}`)
      .then((response) => {
        dispatch({ type: GET_USERS, payload: response.data });
      });
  };
}

export function getUsersCount() {
  return function (dispatch) {
    return axios
      .get("http://localhost:8080/users/get-counts")
      .then((response) => {
        dispatch({ type: GET_USERS_COUNT, payload: response.data });
      });
  };
}
