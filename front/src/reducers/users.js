import { GET_USERS, GET_USERS_COUNT } from "../actions/users";

const initialState = {
  users: [],
  usersCount: 0,
};

export function getAllUsers(state = initialState, action) {
  switch (action.type) {
    case GET_USERS:
      return Object.assign({}, state, { users: action.payload });
    case GET_USERS_COUNT:
      return Object.assign({}, state, { usersCount: action.payload });
    default:
      return state;
  }
}
