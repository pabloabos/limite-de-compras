import { combineReducers } from "redux";
import { getAllUsers } from "./users";

const rootReducer = combineReducers({
  users: getAllUsers,
});

export default rootReducer;
