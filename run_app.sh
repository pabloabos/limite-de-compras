#!/bin/sh

fuser -k 8080/tcp

env | sort

export TNS_PATH="/home/lucas/projects"
export LD_LIBRARY_PATH=/opt/oracle/instantclient_19_8:$LD_LIBRARY_PATH
export DB_USER="stl"
export DB_PASS="stl"
export NODE_ORACLEDB_CONNECTIONSTRING="ARDPROD.WORLD"

FRONT=front 
BACKEND=backend 

cd ${FRONT}
yarn install 
cd ..
cd ${BACKEND}
yarn install 
yarn dev