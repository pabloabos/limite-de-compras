import { combineReducers } from "redux";
import todos from "./todos";
import users from "./users";
import visibilityFilter from "./visibilityFilter";

export default combineReducers({
  todos,
  visibilityFilter,
  users,
});
