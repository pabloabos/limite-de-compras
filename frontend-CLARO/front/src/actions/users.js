export const usersActions = {
  GET_USERS: "GET_USERS",
};

export const getUsers = (users = {}) => ({
  type: "GET_USERS",
  users,
});
