import React from "react";
import { Button } from "@claro/common-ui-components";

const ActionButton = (props) => {
  return (
    <Button onClick={(e) => props.onClick(e.ActionButton)}>{props.name}</Button>
  );
};

export default ActionButton;
