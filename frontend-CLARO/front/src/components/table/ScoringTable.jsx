import React from "react";
import PropTypes from "prop-types";

import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import Checkbox from "@material-ui/core/Checkbox";

function createData(
  name,
  nombre_cli,
  dni,
  riesgo,
  segmento,
  limiteasignar,
  vigenciadesde,
  vigenciahasta,
  usuariocreacion,
  fechaactualizacion,
  usuarioactualizacion
) {
  return {
    name,
    nombre_cli,
    dni,
    riesgo,
    segmento,
    limiteasignar,
    vigenciadesde,
    vigenciahasta,
    usuariocreacion,
    fechaactualizacion,
    usuarioactualizacion,
  };
}

const rows = [
  createData(
    "1",
    "JULIO RIVAROLA",
    "27174432",
    "BAJO",
    "2500",
    "2500",
    "04/08/2020",
    "04/06/2020",
    "Juan Carlos",
    "04/06/2020",
    "Juan Carlos"
  ),
  createData(
    "2",
    "ALEJANDRO MARTINEZ",
    "27174432",
    "MEDIO",
    "300",
    "300",
    "04/08/2020",
    "04/06/2020",
    "STL",
  ),
  createData(
    "3",
    "SILVIA SULER",
    "20271744323",
    "ALTO",
    "2000",
    "2000",
    "04/08/2020",
    "04/06/2020",
    "STL",
    "04/06/2020",
    "STL"
  ),
  createData(
    "4",
    "DIEGO MARADONA",
    "20271744323",
    "BAJO",
    "10",
    "25003",
    "04/06/2020",
    "04/06/2020",
    "STL",
    "04/06/2020",
    "STL"
  ),
  createData(
    "5",
    "SILVIO SOLDAN",
    "27174432",
    "BAJO",
    "25003",
    "25003",
    "04/06/2020",
    "04/06/2020",
    "STL",
    "04/06/2020",
    "STL"
  ),
  createData(
    "6",
    "DXUXA",
    "27174432",
    "BAJO",
    "10",
    "10000",
    "04/06/2020",
    "04/06/2020",
    "STL",
    "04/06/2020",
    "STL"
  ),
  createData(
    "7",
    "PEPITO PEREA",
    "27174432",
    "BAJO",
    "25003",
    "25003",
    "04/06/2020",
    "04/06/2020",
    "STL",
    "04/06/2020",
    "STL"
  ),
];

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

const headCells = [
  {
    id: "name",
    numeric: false,
    disablePadding: true,
    label: "Id. Cliente",
  },
  {
    id: "nombre_cli",
    numeric: true,
    disablePadding: true,
    label: "Nombre/Razón Social",
  },
  { id: "dni", numeric: true, disablePadding: false, label: "Dni/Cuit" },
  { id: "segmento", numeric: true, disablePadding: false, label: "segmento" },
  { id: "Score", numeric: true, disablePadding: false, label: "Score" },
  {
    id: "limiteasignar",
    numeric: true,
    disablePadding: false,
    label: "Límite Asignar",
  },
  {
    id: "vigenciadesde",
    numeric: true,
    disablePadding: false,
    label: "Vigencia Desde",
  },
  {
    id: "vigenciahasta",
    numeric: true,
    disablePadding: false,
    label: "Vigencia Hasta",
  },
  {
    id: "usuariocreacion",
    numeric: true,
    disablePadding: false,
    label: "Usuario Creación",
  },
  {
    id: "fechaactualizacion",
    numeric: true,
    disablePadding: false,
    label: "Fecha Actualización",
  },
  {
    id: "usuarioactualizacion",
    numeric: true,
    disablePadding: false,
    label: "Usuario Actualización",
  },
];

function EnhancedTableHead(props) {
  const { classes, order, orderBy, onRequestSort } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        <TableCell padding="checkbox"></TableCell>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? "right" : "left"}
            padding={headCell.disablePadding ? "none" : "default"}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : "asc"}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === "desc" ? "sorted descending" : "sorted ascending"}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(["asc", "desc"]).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

const useStyles = makeStyles((theme) => ({
  root: {
    width: '99%',

  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  tabla: {
    padding: 7,
  },
}));

export default function EnhancedTable() {
  const classes = useStyles();
  const [order, setOrder] = React.useState("asc");
  const [orderBy, setOrderBy] = React.useState("nombre_cli");
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [dense] = React.useState(false);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = rows.map((n) => n.name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];
    console.log(selectedIndex);
    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }
    setSelected(newSelected);
  };

  const isSelected = (name) => selected.indexOf(name) !== -1;

  const emptyRows =
    rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);

  return (
    <div className={classes.root}>
      {/* <Paper className={classes.paper}> */}
      <Table
        className={classes.tabla}
        aria-labelledby="tableTitle"
        size={dense ? "small" : "medium"}
        aria-label="enhanced table"
      >
        <EnhancedTableHead
          classes={classes}
          numSelected={selected.length}
          order={order}
          orderBy={orderBy}
          onSelectAllClick={handleSelectAllClick}
          onRequestSort={handleRequestSort}
          rowCount={rows.length}
        />
        <TableBody>
          {stableSort(rows, getComparator(order, orderBy))
            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            .map((row, index) => {
              const isItemSelected = isSelected(row.name);
              const labelId = `enhanced-table-checkbox-${index}`;

              return (
                <TableRow
                  hover
                  onClick={(event) => handleClick(event, row.name)}
                  role="checkbox"
                  aria-checked={isItemSelected}
                  tabIndex={-1}
                  key={row.name}
                  selected={isItemSelected}
                >
                  <TableCell padding="checkbox" className={classes.tabla}>
                    <Checkbox
                      checked={isItemSelected}
                      inputProps={{ "aria-labelledby": labelId }}
                    />
                  </TableCell>
                  <TableCell
                    component="th"
                    id={labelId}
                    scope="row"
                    padding="none"
                    className={classes.tabla}
                  >
                    {row.name}
                  </TableCell>
                  <TableCell className={classes.tabla} align="right">{row.nombre_cli}</TableCell>
                  <TableCell className={classes.tabla} align="right">{row.dni}</TableCell>
                  <TableCell className={classes.tabla} align="right">{row.segmento}</TableCell>
                  <TableCell className={classes.tabla} align="right">{row.riesgo}</TableCell>
                  <TableCell className={classes.tabla} align="right">{row.limiteasignar}</TableCell>
                  <TableCell className={classes.tabla} align="right">{row.vigenciadesde}</TableCell>
                  <TableCell className={classes.tabla} align="right">{row.vigenciahasta}</TableCell>
                  <TableCell className={classes.tabla} align="right">{row.usuariocreacion}</TableCell>
                  <TableCell className={classes.tabla} align="right">
                    {row.fechaactualizacion}
                  </TableCell>
                  <TableCell align="right" className={classes.tabla}>
                    {row.usuarioactualizacion}
                  </TableCell>
                </TableRow>
              );
            })}
          {emptyRows > 0 && (
            <TableRow style={{ height: (dense ? 33 : 53) * emptyRows }}>
              <TableCell colSpan={6} className={classes.tabla} />
            </TableRow>
          )}
        </TableBody>
      </Table>

    </div >
  );
}
