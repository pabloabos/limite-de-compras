import React, { useState } from "react";
import {
  Typography,
  Colors,
  Input,
  InputActions,
  Search,
  Cancel,
  SaveIcon,
  TransparentBackIconButton,
  RefreshIcon
} from "@claro/common-ui-components";
import Grid from "@material-ui/core/Grid";
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(() => ({
  root: {
    "& .MuiListItem-root": {
      paddingTop: "0px",
      paddingBottom: "0px",
    }
  },
  ppal: {
    "& .MuiList-padding ": {
      paddingTop: "0px",
      paddingBottom: "0px",
    }
  },
  margin: {
    margin: "6px"
  },

}));


const SearchClient = () => {
  const [value, setValue] = useState("");
  const classes = useStyles();
  const updateState = (event) => {
    setValue(event.target.value);
  };
  const cleanState = (event) => {
    setValue("");
  };
  return (
    <>
      <Typography Component="h5" style={{ color: Colors.RojoBackground }}>
        Búsqueda por Cliente
      </Typography>

      <Grid container className={classes.ppal} >
        <Grid item xs={7} >
          <Input
            id="buscadorCliente"
            type="number"
            labelText="Cod Cli  Dni  Cuit"
            rounded
            autoFocus
            className={classes.margin}
            onChange={updateState}
            value={value}
          >
            <InputActions>
              {value !== "" ? (
                <TransparentBackIconButton
                  Component="button"
                  onClick={cleanState}
                  transparentBack="true"
                >
                  <Cancel size={18} />
                </TransparentBackIconButton>
              ) : undefined}
              <TransparentBackIconButton
                Component="button"
                onClick={() => alert(value)}
                transparentBack="true"
              >
                <Search size={21} />
              </TransparentBackIconButton>
            </InputActions>
          </Input>
        </Grid>
        <List className={classes.root}>
          <ListItem button onClick={() => alert("Click en boton refrescar Grilla")} >
            <RefreshIcon size={22} />
            <ListItemText primary="Refrescar Grilla" />
          </ListItem>
          <ListItem button onClick={() => alert("Click en boton  Guardar Cliente")}>
            <SaveIcon size={22} />
            < ListItemText primary="Guardar Cliente" />
          </ListItem>
        </List>
      </Grid >
    </>
  );
};

export default SearchClient;
