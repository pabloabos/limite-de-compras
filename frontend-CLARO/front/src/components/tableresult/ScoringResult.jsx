import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import ActionButton from "../button/Button";
import Grid from "@material-ui/core/Grid";
import { Typography } from "@claro/common-ui-components";
import { makeStyles } from "@material-ui/core/styles";
const nombreBotonArchivo = ["Procesar "];
const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.white,
    color: theme.palette.common.black,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const useStyles = makeStyles(() => ({
  tabla: {
    padding: 3,
  },
}));

function createData(Procesados, Correctos, Incorrectos) {
  return { Procesados, Correctos, Incorrectos };
}

const rows = [createData(0, 0, 0)];

export default function ScoringResult() {
  const classes = useStyles();
  return (
    <>

      <Typography Component="h5"> Procesar Archivo</Typography>
      <Grid container spacing={1} >
        <Grid item xs={11}>
          <TableContainer component={Paper}>
            <Table aria-label="customized table">
              <TableHead>
                <TableRow>
                  <StyledTableCell className={classes.tabla} align="right">Procesados</StyledTableCell>
                  <StyledTableCell className={classes.tabla} align="right">Correctos</StyledTableCell>
                  <StyledTableCell className={classes.tabla} align="right">Incorrectos</StyledTableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {rows.map((row) => (
                  <StyledTableRow key={row.Procesados}>
                    <StyledTableCell className={classes.tabla} align="right">
                      {row.Procesados}
                    </StyledTableCell>
                    <StyledTableCell className={classes.tabla} align="right">
                      {row.Correctos}
                    </StyledTableCell>
                    <StyledTableCell className={classes.tabla} align="right">
                      {row.Incorrectos}
                    </StyledTableCell>
                  </StyledTableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>
        <Grid item >
          {nombreBotonArchivo.map((row, index) => (
            <ActionButton
              key={index}
              onClick={() => console.log(`Click en boton ${row}`)}
              name={row}
            />
          ))}
        </Grid>
      </Grid>
    </>
  );
}
