import React from "react";
import LogoClaro from "../Logo/Claro_logo.png";
import { AppBar, AppBarTitle, AppBarLogo } from "@claro/common-ui-components";
import Grid from "@material-ui/core/Grid";

const Header = () => {
  return (
    <AppBar
      style={{
        position: "relative",
      }}
    >
      <Grid container spacing={1}>
        <AppBarLogo
          src={LogoClaro}
          width={65}
          height={25}
          alt="Claro"
          onClick={() => alert("Volver")}
        />
        <AppBarTitle>Excepción Límite de Compras</AppBarTitle>
      </Grid>
    </AppBar>
  );
};

export default Header;
