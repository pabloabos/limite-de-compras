import React, { useState } from "react";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import {
  Typography,
  Colors,
  Input,
  InputActions,
  Search,
  Cancel,
  TransparentBackIconButton,
} from "@claro/common-ui-components";
import Modal from "../modal/Modal"
const useStyles = makeStyles((theme) => ({
  root: {
    "& .MuiTextField-root": {
      margin: theme.spacing(1),
      width: "20ch",
    },
  },
  busqueda: {
    margin: theme.spacing(1),
  },
}));


const SearchArchive = () => {
  const [value, setValue] = useState("");
  const classes = useStyles();
  const updateState = (event) => {
    setValue(event.target.value);
  };
  const cleanState = (event) => {
    setValue("");
  };
  return (
    <>
      <Typography Component="h5" style={{ color: Colors.RojoBackground }}>
        Carga Masiva
        </Typography>
      <Grid container className={classes.busqueda} spacing={1}>
        <Grid item xs={10}>
          <Input
            id="buscadorArchivo"
            type="text"
            labelText="Nombre del Archivo (.txt)"
            rounded
            autoFocus
            onChange={updateState}
            value={value}
          >
            <InputActions>
              {value !== "" ? (
                <TransparentBackIconButton
                  Component="button"
                  onClick={cleanState}
                  transparentBack="true"
                >
                  <Cancel size={18} />
                </TransparentBackIconButton>
              ) : undefined}
              <TransparentBackIconButton
                Component="button"
                onClick={() => alert(value)}
                transparentBack="true"
              >
                <Search size={20} />
              </TransparentBackIconButton>
            </InputActions>
            <Modal />
          </Input>

        </Grid>
      </Grid>
    </>
  );
};
export default SearchArchive;
