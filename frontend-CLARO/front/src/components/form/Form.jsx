import React from "react";
import Grid from "@material-ui/core/Grid";
import SearchArchive from "../formarchive/SearchArchive";
import SearchClient from "../formClient/SearchClient";
import ScoringResult from "../tableresult/ScoringResult";

const FileForm = () => {
  return (
    <>
      <Grid container >
        <Grid item xs={4} >
          <SearchClient />
        </Grid>
        <Grid item xs={5}>
          <SearchArchive />
        </Grid>
        <Grid item xs={3} >
          <ScoringResult />
        </Grid>
      </Grid>
    </>
  );
};

export default FileForm;
