import React from "react";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";

import ScoringTable from "../components/table/ScoringTable";
import FileForm from "../components/form/Form";

const useStyles = makeStyles((theme) => ({
  tabla: {
    padding: 68,
  },
}));

const MainContainer = () => {
  const classes = useStyles();
  return (
    <Grid container >
      <Grid item xs={12} className={classes.tabla}>
        <FileForm />
      </Grid>
      <Grid item xs={12}>
        <ScoringTable />
      </Grid>
    </Grid >
  );
};

export default connect()(MainContainer);
