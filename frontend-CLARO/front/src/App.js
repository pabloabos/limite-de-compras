import React from "react";
import Grid from "@material-ui/core/Grid";
import Main from "./containers/Main";
import Header from "./components/header/Header";
import "./App.css";

function App() {
  return (
    <div className="App">
      <Grid container >
        <Grid item xs={12}>
          <Header />
        </Grid>
        <Grid item xs={12}>
          <Main />
        </Grid>
      </Grid>
    </div>
  );
}

export default App;
