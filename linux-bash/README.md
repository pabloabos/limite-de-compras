# Linux VPN configuration

wget https://starkers.keybase.pub/snx_install_linux30.sh?dl=1 -O snx_install.sh

Run the following commands on your CLI in order to get the VPN working on Linux

```
1. sudo dpkg --add-architecture i386
2. sudo apt-get update
3. sudo apt-get install libstdc++5:i386 libx11-6:i386 libpam0g:i386
4. chmod a+rx snx_install.sh
5. sudo ./snx_install.sh
6. sudo ldd /usr/bin/snx
7. snx -s 131.100.108.102 -u <YOUR_EXA_USR>
```

After running the last command, you will have to enter your token from RSAID and that's it, you are connected