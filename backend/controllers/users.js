/**
 * return the users rows selected by the user
 * @param {object} req
 * @param {object} res
 * @param {object} oracledb
 */
async function getAllUsers(req, res, oracledb) {
  try {
    connection = await oracledb.getConnection({
      user: process.env.DB_USER || "stl",
      password: process.env.DB_PASS || "stl",
      connectString:
        process.env.NODE_ORACLEDB_CONNECTIONSTRING || "ARDPROD.WORLD",
    });

    console.log("connected to database");
    // run query to get all users
    result = await connection.execute(
      `SELECT USR_ID, USR_NAME, USR_SURNAME,
      USR_MAIL_ADDRESS, USR_START_DATE, 
      USR_END_DATE 
        FROM (SELECT A.*, rownum rn
        FROM (SELECT * FROM users ORDER BY USR_ID) A
        WHERE rownum <= ${req.query.to})
      WHERE rn >= ${req.query.from}`,
      [], // no binds
      {
        outFormat: oracledb.OBJECT,
      }
    );
  } catch (err) {
    //send error message
    return res.send(err.message);
  } finally {
    if (connection) {
      try {
        // Always close connections
        // await connection.close();
        console.log("close connection success");
      } catch (err) {
        console.error(err.message);
      }
    }
    if (result.rows.length == 0) {
      //query return zero employees
      return res.send("query send no rows");
    } else {
      //send all employees
      return res.send(result.rows);
    }
  }
}

/**
 * return the users rows selected by the user
 * @param {object} req
 * @param {object} res
 * @param {object} oracledb
 */
async function getUsersCount(req, res, oracledb) {
  try {
    connection = await oracledb.getConnection({
      user: process.env.DB_USER || "stl",
      password: process.env.DB_PASS || "stl",
      connectString:
        process.env.NODE_ORACLEDB_CONNECTIONSTRING || "ARDPROD.WORLD",
    });

    console.log("connected to database");
    // run query to get all users
    result = await connection.execute(
      `SELECT count(*) AS "usersNumber" FROM users`,
      [], // no binds
      {
        outFormat: oracledb.OBJECT,
      }
    );
  } catch (err) {
    //send error message
    return res.send(err.message);
  } finally {
    if (connection) {
      try {
        // Always close connections
        // await connection.close();
        console.log("close connection success");
      } catch (err) {
        console.error(err.message);
      }
    }
    if (result.rows.length == 0) {
      //query return zero employees
      return res.send("query send no rows");
    } else {
      //send all employees
      return res.send(result.rows);
    }
  }
}

module.exports.getAllUsers = getAllUsers;
module.exports.getUsersCount = getUsersCount;
