const createError = require("http-errors");
const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const oracledb = require("oracledb");

// get the configuration from your tnsnames.ora file
oracledb.initOracleClient({ configDir: process.env.TNS_PATH });

const indexRouter = require("./routes/index");
const usersController = require("./controllers/users");
const app = express();

console.log(usersController.getUsersCount);

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
//CORS Settings
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://localhost:3000"); // update to match the domain you will make the request from
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

// Routes and controllers definition
app.use("/", indexRouter);
app.get("/users", function (req, res) {
  usersController.getAllUsers(req, res, oracledb);
});
app.get("/users/get-counts", function (req, res) {
  usersController.getUsersCount(req, res, oracledb);
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
