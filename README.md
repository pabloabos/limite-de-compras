# Exception Limit

## Before you start

- install _nodeJS_ on your machine [nodejs](https://nodejs.org/es/)
- install oracleDB on your machine [oracleDB](https://oracle.github.io/node-oracledb/INSTALL.html#quickstart)
- install sqlPlus on your machine [sqlPlus](https://docs.oracle.com/cd/B19306_01/server.102/b14357/qstart.htm)

```
sudo mkdir -p /opt/oracle
cd /opt/oracle
sudo wget https://download.oracle.com/otn_software/linux/instantclient/instantclient-basic-linuxx64.zip
sudo unzip instantclient-basic-linuxx64.zip
```

You will need the operating system _libaio_ package installed. On some platforms the package is called _libaio1_. Run a command like `sudo apt install -y libaio` or `sudo apt install -y libaio1`

If there is no other Oracle software on the machine that will be impacted, then permanently add Instant Client to the run-time link path. For example, if the Basic package unzipped to **/opt/oracle/instantclient_19_x**, then run the following using sudo or as the root user:

```
sudo sh -c "echo /opt/oracle/instantclient_19_6 > /etc/ld.so.conf.d/oracle-instantclient.conf"
sudo ldconfig
```

Alternatively, every shell running Node.js will need to have the link path set:

`export LD_LIBRARY_PATH=/opt/oracle/instantclient_19_6:$LD_LIBRARY_PATH`

After that, check your TNS config on the file: _backend/app.js_

To add the ENV variable, you will have to update the line on the **run_app.sh**: `export TNS_PATH=/home/lucas/projects` with your TNS_PATH like you have on this example

Also, create this ENV variable `export LD_LIBRARY_PATH=/opt/oracle/instantclient_19_8:$LD_LIBRARY_PATH`

Check if the variable exist with the command `env` on your console

### ENV variables

you can just run `set_variables.sh` script in order to create all the **env variables you need** (if the script don't work, you can copy and past the rows in the console)

Then, just check this line is ok

`oracledb.initOracleClient({ configDir: process.env.TNS_PATH });`

## Running the application

For starting with the exception_limit_react you will have to give execution permissions to the file **run_app**

`chmod +x run_app.sh`

After that you can run `./run_app.sh` on your console.

That will install all the backend and frontend libraries you need, and also will start the app

- localhost:3000 _for frontend_
- localhost:8080 _for backend_
